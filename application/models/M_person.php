<?php

class M_person extends CI_Model
{
    public function get_all_person($id=NULL)
    {
        if ($id===NULL) {
            
            $hsl= $this->db->get('person');
            return $hsl->result_array();    
        }else {
            $hsl=$this->db->get_where('person', ['id'=>$id]);
            return $hsl->result_array();
        }
    }

    public function hapus($id)
    {
        $this->db->delete('person', ['id'=> $id]);
        return $this->db->affected_rows();
    }
    public function simpan($data)
    {
        $this->db->insert('person', $data);
        return $this->db->affected_rows();
    }

    public function update($data, $id)
    {
        $this->db->update('person', $data, ['id'=>$id]);
        return $this->db->affected_rows();
    }

}