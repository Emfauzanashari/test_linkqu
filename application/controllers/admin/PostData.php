<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PostData extends CI_Controller {

	public function post()
	{
		$data=$this->input->post();
		$age=explode(' ',$data['age']);

		$insert=array(
			'name'=> strtoupper($data['name']),
			'age'=> $age[0],
			'city'=>strtoupper($data['city']),
		);
		// var_dump($insert);
		// die();
		$insert_db=$this->db->insert('person', $insert);

		if ($insert_db) {
			$array=array(
				'status' => 'success',
				'message'=> 'success Insert data',
				'data' =>	1
			);
		}else{
			$array=array(
				'status' => 'Failed',
				'message'=> 'Failed to Insert data',
				'data'	=>	0
			);
		}

		$this->output->set_content_type('application/json');
	    $this->output->set_output(json_encode($array));
	}
}
