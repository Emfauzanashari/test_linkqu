<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Person extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('m_person');

    }

    //Menampilkan data person
   public function index_get() {
            $id = $this->get('id');
            // var_dump($id);
            // die();
            if ($id = null) {
                $person=$this->db->get('person')->result();
                $this->response($person, REST_Controller::HTTP_OK); 
        
            } else {
                $person=$this->m_person->get_all_person($id);
            }
            if ($person) {
                    $this->response([
                            'status' => TRUE,
                            'data' => $person
                    ], REST_Controller::HTTP_OK);
            }else {
                $this->response([
                             'status' => FALSE,
                             'message' => 'ID Not found'
                    ], REST_Controller::HTTP_NOT_FOUND);
            }
             
        }

        public function index_delete(){
        $id =  $this->delete('id');

        // Validate the id.
        if ($id == null)
        {
            $this->response([
                             'status' => FALSE,
                             'message' => 'ID Tidak Boleh Kosong'
                    ], REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }else {
            if ($this->m_person->hapus($id)>0) {
                //hapus
                $this->response([
                            'status' => TRUE,
                            'id' => $id,
                            'message'=>' Terhapus'
                    ], REST_Controller::HTTP_OK);
            }else {
                $this->response([
                             'status' => FALSE,
                             'message' => 'ID Not found'
                    ], REST_Controller::HTTP_NO_CONTENT);
            }
        }

    }

    public function index_post(){
        $data=$this->input->post();
        // var_dump($data);
        // die();
        $age=explode(' ',$data['age']);

        $insert=array(
            'name'=> strtoupper($data['name']),
            'age'=> $age[0],
            'city'=>strtoupper($data['city']),
        );
        if ($this->m_person->simpan($insert)>0) {
                        $this->response([
                            'status' => TRUE,
                            'message'=>'Data Tersimpan'
                    ], REST_Controller::HTTP_CREATED);
        }else {
                        $this->response([
                             'status' => FALSE,
                             'message' => 'Gagal Menambahkan Data'
                    ], REST_Controller::HTTP_BAD_REQUEST);
    }
}

    public function index_put(){
        $id=$this->put('id');
           $data=$this->put();
        $age=explode(' ',$data['age']);

        $update=array(
            'name'=> strtoupper($data['name']),
            'age'=> $age[0],
            'city'=>strtoupper($data['city']),
        );
        if ($this->m_person->update($update, $id)>0) {
                        $this->response([
                            'status' => TRUE,
                            'message'=>'Data Terupdate'
                    ], REST_Controller::HTTP_OK);
        }else {
                        $this->response([
                             'status' => FALSE,
                             'message' => 'Gagal Mengupdate Data'
                    ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }
        
    }

?>