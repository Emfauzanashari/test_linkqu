<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Form Submit</title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	
</head>
<body>
	<section>
		<div class="container">
			<div class="row" style="justify-content: space-around;">
				<h2 class="text-center">Form Input Person</h2>
			</div>
			<div class="row">
				<div class="col-xm-12 col-sm-12">
					<form id="formPerson">
						<div class="form-group">
							<label >Name</label>
							<input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" required>
						</div>
						<div class="form-group">
							<label >Age</label>
							<input type="text" class="form-control" name="age" onkeypress="return hanyaAngka(event)" id="age" placeholder="Age" autocomplete="off" required>
						</div>
						<div class="form-group">
							<label >City</label>
							<input type="text" class="form-control" name="city" id="city" placeholder="City" required>
						</div>
						
						<button type="submit" id="submit" class="btn btn-primary">Submit</button>
					</form>
				</div>
			</div>
		</div>
	</section>

</body>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script src="https://code.jquery.com/jquery-3.6.4.min.js" integrity="sha256-oP6HI9z1XaZNBrJURtCoUT5SUnxFr8s3BzRl+cbzUq8=" crossorigin="anonymous"></script>
	
<script>
	function hanyaAngka(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
 
		    return false;
		  return true;
		}

	$(document).ready(function() {
		$("#formPerson").submit(function(e) {
          e.preventDefault();
          var formData = $(this).serialize();
          console.log(formData);
          $.ajax({
              url: '<?=base_url().'post'?>',
              type: 'post',
              data: formData, 
              dataType: "json",

               success: function(response){
            	console.log(response);
            	if (response.data==1) {
            		 alert("Data success To Input");
            		 $('#formPerson')[0].reset();
            	}else{
            		alert("Data Failed To Input");
            		 $('#formPerson')[0].reset();

            	}
        }
          });
      });
	});
</script>

</html>